$(document).ready(function () {
    setInterval(getLastKey, 1000);
    setInterval(getHistoric, 1000);
});

var startKey = '0000';

function getLastKey() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: 'http://paripassu.dev.com/lastKey',
        success: function (data) {
            updateScreen(data);
        },
        error: function (er, te, error) {
            console.log(error)
        }
    });
}

function updateScreen(lastKey) {

    if (lastKey === null || lastKey === '') {
        $('#input_key').val(startKey);

    } else {

        $('#input_key').empty();

        $('#input_key').fadeOut(1000, function () {
            $('#input_key').val(lastKey).fadeIn(1000);
        });
    }
}

function getHistoric() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: 'http://paripassu.dev.com/historic',
        success: function (data) {
            showHistoric(data);
        },
        error: function (er, te, error) {
            console.log(error)
        }
    });
}

function showHistoric(historic) {

    if (historic !== ''){

        $('#historic').empty();

        historic.forEach(function (item, indice, array) {

            $html =
                '<div class="row">' +
                '<div class="col-md-3">' +
                '   <label class="key">' + item['Senha'] + '</label>\n' +
                '</div>' +
                '<div class="col-md-3">' +
                '   <label class="station">' + item['Guichê'] + '</label>\n' +
                '</div>\n' +
                '<div class="col-md-3">\n' +
                '   <label class="time">' + item['Hora'] + '</label>\n' +
                '</div>' +
                '</div>';

            $('#historic').append($html);
        });
    }
}

