
$(document).ready(function () {
    callLastKey();
    clearKeys();
});

var startKey = '0000';

function clearKeys() {

    $('#clear_keys').click(function () {

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: 'http://paripassu.dev.com/clear',
            success: function (data) {
                location.reload();
                console.log(data);
            },
            error: function (er, te, error) {
                console.log(error)
            }
        });
    });
}

function callLastKey() {

    $('#call_last_key').click(function () {

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: 'http://paripassu.dev.com/callKey',
            success: function (data) {
                console.log(data);
            },
            error: function (er, te, error) {
                console.log(error);
            }
        });
    });
}
