<?php

function sessions($session){

    $values = [
        'lastKeyP'      => is_null(Session::get('lastKeyP'))      ? 0 : Session::get('lastKeyP'),
        'preferenthial' => is_null(Session::get('preferenthial')) ? 0 : Session::get('preferenthial'),
        'normal'        => is_null(Session::get('normal'))        ? 0 : Session::get('normal'),
        'lastKeyN'      => is_null(Session::get('lastKeyN'))      ? 0 : Session::get('lastKeyN'),
    ];

    return $values[$session];
}
