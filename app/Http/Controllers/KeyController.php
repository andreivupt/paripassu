<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KeyController extends Controller
{
    public $lastKey       = null;
    public $lastKeyP      = null;
    public $lastKeyN      = null;
    public $preferenthial = 0;
    public $normal        = 0;

    /**
     * Tela para os requisitos funcionais Nºs 2 e 3:
     * - Deve ser possível acompanhar a chamada das senhas;
     * - O acesso ao acompanhamento e a geração de novas senhas deve ser público;
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function show()
    {
        return view('screen.index');
    }

    /**
     * Tela para os requisitos Nºs 5 e 6:
     * - Somente o GERENTE será capaz de chamar próximas senhas;
     * - Não há necessidade de login e senha para o perfil de GERENTE;
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manager()
    {
        return view('manager.index');
    }

    /**
     * Tela para o requisito Nº 3 (Geração de novas senhas):
     * - O acesso ao acompanhamento e a geração de novas senhas deve ser público;
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Chama próxima senha
     * Método para os requisitos funcionais Nºs 1 e 4:
     * - Deve ser possível gerar novas senhas, que podem ser de dois tipos: NORMAL e
     *   PREFERENCIAL, com os formatos “N####” e “P####” respectivamente, onde “#” é um
     *   dígito;
     *
     * @return string
     *
     */
    public function callLastKey()
    {
        // Recupera os valores da sessão
        $lastKeyP       = sessions('lastKeyP');
        $preferenthial  = sessions('preferenthial');
        $normal         = sessions('normal');
        $lastKeyN       = sessions('lastKeyN');

        if ($preferenthial != 0 && $preferenthial > $lastKeyP) {
            $newKeyPreferenthial = $lastKeyP + 1;
            $type                = 'P';
            $lastKey             = $this->formatLastKey($newKeyPreferenthial);

        } else if ($normal != 0 && $normal > $lastKeyN) {
            $newKeyNormal = $lastKeyN + 1;
            $type         = 'N';
            $lastKey      = $this->formatLastKey($newKeyNormal);
        }

        // Atualiza informações na sessão
        $this->updateSession($lastKey, $type);

        return 'succes';
    }

    /**
     * Gerador de senha
     *
     * @param Request $request
     * @return string
     */
    public function setKey(Request $request)
    {
        // Pega o tipo de senha enviada pelo AJAX
        $type = $request->type;
        // Recupera sessão do tipo solicitado
        $result = $this->getSession($type);
        // Se a sessão for nula inicia contagem, senão acrescenta
        $hasKey = is_null($result) ? 1 : $result = $result + 1;
        // Atualiza informações na sessão
        $this->setKeysSession($hasKey, $type);

        return 'success';
    }

    /**
     * Recupera ultima senha preferencial
     *
     * @param $name
     * @return mixed
     */
    public function getSession($name)
    {
        return Session::get($name);
    }

    /**
     * Insere solicitação de senha na sessão
     *
     * @param $lastKey
     * @param $value
     * @param $type
     */
    public function setKeysSession($value, $type)
    {
        Session::put($type, $value);
    }

    /**
     * Atualiza informações na sessão
     *
     * @param $lastKey
     * @param $type
     *
     */
    public function updateSession($lastKey, $type)
    {
        $historic = Session::get('historic');

        $data = array(
            'Senha'  => $type . $lastKey,
            'Guichê' => '01',
            'Hora'   => date('H:m:s')
        );

        if ($historic != '') {
            array_push($historic, $data);
        } else {
            $historic = array($data);
        }

        Session::put('lastKey' . $type, $lastKey);
        Session::put('lastKey', $type . $lastKey);
        Session::put('historic', $historic);
    }

    /**
     * Gera padrão de senha
     *
     * @param $key
     * @return string
     */
    public function formatLastKey($key)
    {
        return str_pad($key, 4, '0', STR_PAD_LEFT);
    }

    /**
     *  Retorna última senha gerada na sessão
     *
     * @return mixed
     */

    public function getKey()
    {
        return Session::get('lastKey');
    }

    /**
     *  Retorna historico senhas
     *
     * @return mixed
     */
    public function showHistoric()
    {
        return Session::get('historic');
    }

    /**
     * Redefine as variaveis de sesão
     */
    public function clearKeys()
    {
        Session::put('lastKey', null);
        Session::put('lastKeyP', null);
        Session::put('lastKeyN', null);
        Session::put('normal', null);
        Session::put('preferenthial', null);
        Session::put('historic', '');

    }
}
