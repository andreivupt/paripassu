<?php

Route::get('test', function (){

    dd(\Illuminate\Support\Facades\Session::all());

});

Route::group(['as' => 'views'], function (){
    // Tela para selecionar senha
    Route::get('/','KeyController@index')->name('index');
    // Tela para chamar proxima senha
    Route::get('admin','KeyController@manager')->name('manager');
    // Exibe senhas
    Route::get('show','KeyController@show')->name('show');
});

Route::group(['as' => 'ajax'], function () {
    // Atualiza última senha solicitada
    Route::post('setKey', 'KeyController@setKey');
    // Recupera ultima senha gerada
    Route::post('lastKey', 'KeyController@getKey');
    // Recupera histórico de senhas
    Route::post('historic', 'KeyController@showHistoric');
    // Atualiza chamada
    Route::post('callKey', 'KeyController@callLastKey');
    // Atualiza chamada
    Route::post('clear', 'KeyController@clearKeys');
});

