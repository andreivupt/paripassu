<!DOCTYPE html>
<html lang="en">
<head>
    <title>Painel de senhas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/style.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}} ">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/util.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
<body>
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row container-fluid">
    <div class="col-md-8 container-login100">

        <h2>Painel de Senhas</h2>
        <div class="col-md-12 wrap-login100 p-t-55 p-b-55">
            <div class="row">
                <div class="col-md-6">
                    <div class="key-label center-div">
                        <h4>Senha</h4>
                    </div>
                    <div class="col-md-12 center-div">
                        <div class="center-div block-content">
                            <input type="text" class="input-key" value="0000" id="input_key">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="key-label center-div">
                        <h4>Guichê</h4>
                    </div>
                    <div class="col-md-12 center-div">
                        <div class="center-div block-content">
                            <input type="text" class="input-key" value="01">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-4 container-login100">
        <h2>Histórico de Senhas</h2>
        <div class="col-md-12 wrap-login100 p-t-55 p-b-55 ">
            <div class="row">
                <div class="col-md-3">
                    <label><strong>Senha</strong></label>
                </div>
                <div class="col-md-3">
                    <label><strong>Guichê</strong></label>
                </div>
                <div class="col-md-3">
                    <label><strong>Hora</strong></label>
                </div>
            </div>
            <div class="scroll" id="historic" style="height: 135px;">

            </div>
        </div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/screenView.js') }}"></script>

</body>
</html>
