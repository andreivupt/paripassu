$(document).ready(function () {
    requestKey();
});

function requestKey() {

    $('#prefenthialKey').click(function () {
        setKey('preferenthial');
    });

    $('#nomalKey').click(function () {
        setKey('normal');
    });
}

function setKey(type) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        data: {'type': type}, //'preferenthial'},
        url: 'http://paripassu.dev.com/setKey',
        success: function (data) {
            console.log(data);
        },
        error: function (er,te,error) {
            console.log(error);
        }
    });

}

