<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gerenciador de Senhas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/style.css') }}">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}} ">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/util.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="container">
    <div class="container-login100">

        <div class="row wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
            <div class="col-md-10 container-login100-form-btn">
                <button class="btn btn-primary" id="call_last_key">Próxima senha</button>
            </div>
            <div class="col-md-2 container-login100-form-btn">
                <button class="btn btn-danger" id="clear_keys">Redefinir senhas</button>
            </div>
        </div>

    </div>
</div>
<script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/updateScreen.js') }}"></script>

</body>
</html>
